#/usr/bin/env python
# -*- coding: utf8 -*-
# title       :
# description :
# author      :'ShenMeng'

import maya.cmds as cmds
import os

def main():
    scene_name = cmds.file(q=1,sn=1)
    if not scene_name:
        return "scenName: untitled"
    base_name = os.path.basename(scene_name)
    return base_name

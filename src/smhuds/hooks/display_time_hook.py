#/usr/bin/env python
# -*- coding: utf8 -*-
# title       :
# description :
# author      :'ShenMeng'

import time

def main():
    ctime = time.strftime(ur"%Y/%m/%d 周{%a} %H:%M:%S".encode('gbk'))
    nowTime = ctime.format(Mon=ur"一".encode('gbk'),
                           Tue=ur"二".encode('gbk'),
                           Wed=ur"三".encode('gbk'),
                           Thu=ur"四".encode('gbk'),
                           Fri=ur"五".encode('gbk'),
                           Sat=ur"六".encode('gbk'),
                           Sun=ur"日".encode('gbk'))
    return nowTime

#/usr/bin/env python
# -*- coding: utf8 -*-
# title       :
# description :
# author      :'ShenMeng'

import maya.cmds as cmds

def main():
    cam_focal = cmds.getAttr(cmds.lookThru(q=1)+".focalLength")
    return "FL: %.1f" % cam_focal
    
